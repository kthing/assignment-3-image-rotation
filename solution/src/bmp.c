#include "bmp.h"
#include <stdlib.h>


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    img->data = (struct pixel *) malloc(img->width * img->height * sizeof(struct pixel));
    if (!img->data) {
        return READ_OUT_OF_MEMORY;
    }

    fseek(in, (long)header.bOffBits, SEEK_SET);

    for (int y = (int)(img->height) - 1; y >= 0; y--) {
        for (int x = 0; x < img->width; x++) {
            struct pixel pix;
            if (fread(&pix, sizeof(struct pixel), 1, in) != 1) {
                free_image(img);
                return READ_INVALID_BITS;
            }
            img->data[y * img->width + x] = pix;
        }
        fseek(in, (long)((4 - (img->width * sizeof(struct pixel)) % 4) % 4), SEEK_CUR);

    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, const struct image *img) {
    struct bmp_header header;

    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24; // 24-bit RGB :))
    header.biCompression = 0;
    header.biSizeImage = img->width * img->height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    for (int y = (int)(img->height) - 1; y >= 0; y--) {
        for (int x = 0; x < img->width; x++) {
            if (fwrite(&img->data[y * img->width + x], sizeof(struct pixel), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
        for (int i = 0; i < (4 - (img->width * sizeof(struct pixel)) % 4) % 4; i++) {
            if (fputc(0, out) == EOF) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
