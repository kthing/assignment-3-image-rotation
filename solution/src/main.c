#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>



int main(int argc, char **argv) {
    if (argc != 4) {
        printf("Something is wrong! \nUsage: ./image-transformer <source-image> <transformed-image> <angle>\n");
        return 1;
    }

    FILE *source_file = fopen(argv[1], "rb");
    FILE *transformed_file = fopen(argv[2], "wb");

    if (!source_file || !transformed_file) {
        printf("Error: Unable to open files.\n");
        return 1;
    }

    int angle = atoi(argv[3]);

    struct image source_image;
    if (from_bmp(source_file, &source_image) != READ_OK) {
        printf("Error: Failed to read source image.\n");
        fclose(source_file);
        fclose(transformed_file);
        return 1;
    }

    struct image transformed_image = rotate(source_image, angle);

    if (to_bmp(transformed_file, &transformed_image) != WRITE_OK) {
        printf("Error: Failed to write transformed image.\n");
        free_image(&transformed_image);
        fclose(source_file);
        fclose(transformed_file);
        return 1;
    }

    free_image(&transformed_image);
    fclose(source_file);
    fclose(transformed_file);

    return 0;
}
