#include "bmp.h"
#include "pixel.h"
#include <stdlib.h>


struct image rotate(const struct image source, int angle) {
    angle %= 360;
    if (angle < 0)
        angle += 360;

    struct image rotated_image;
    rotated_image.width = source.height;
    rotated_image.height = source.width;
    rotated_image.data = (struct pixel *) malloc(rotated_image.width * rotated_image.height * sizeof(struct pixel));

    if (!rotated_image.data) {
        rotated_image.width = 0;
        rotated_image.height = 0;
        return rotated_image;
    }

    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            uint64_t new_x, new_y;

            switch (angle) {
                case 90:
                    new_x = y;
                    new_y = source.width - x - 1;
                    break;
                case 180:
                    new_x = source.width - x - 1;
                    new_y = source.height - y - 1;
                    break;
                case 270:
                    new_x = source.height - y - 1;
                    new_y = x;
                    break;
                default:
                    new_x = x;
                    new_y = y;
                    break;
            }

            rotated_image.data[new_y * rotated_image.width + new_x] = source.data[y * source.width + x];
        }
    }

    return rotated_image;
}
